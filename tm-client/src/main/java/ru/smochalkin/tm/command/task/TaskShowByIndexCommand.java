package ru.smochalkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractTaskCommand;
import ru.smochalkin.tm.endpoint.TaskDto;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-show-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.print("Enter index: ");
        @NotNull Integer index = TerminalUtil.nextInt();
        @NotNull final TaskDto task = serviceLocator.getTaskEndpoint()
                .findTaskByIndex(serviceLocator.getSession(), --index);
        showTask(task);
    }

}
