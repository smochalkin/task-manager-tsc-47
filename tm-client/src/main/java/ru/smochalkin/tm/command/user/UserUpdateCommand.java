package ru.smochalkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.command.AbstractCommand;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.exception.empty.EmptyObjectException;
import ru.smochalkin.tm.exception.system.AccessDeniedException;
import ru.smochalkin.tm.util.TerminalUtil;

public class UserUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "user-update";
    }

    @Override
    @NotNull
    public String description() {
        return "Update a user.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new EmptyObjectException();
        if (serviceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.println("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final Result result = serviceLocator.getUserEndpoint()
                .userUpdate(serviceLocator.getSession(), firstName, lastName, middleName);
        printResult(result);
    }

}
