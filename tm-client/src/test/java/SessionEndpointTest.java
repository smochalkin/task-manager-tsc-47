import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.endpoint.Result;
import ru.smochalkin.tm.endpoint.SessionDto;
import ru.smochalkin.tm.endpoint.SessionEndpoint;
import ru.smochalkin.tm.endpoint.SessionEndpointService;
import ru.smochalkin.tm.marker.SoapCategory;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test
    @Category(SoapCategory.class)
    public void openSessionTest() {
        Assert.assertNotNull(sessionEndpoint.openSession("admin", "100"));
    }

    @Test
    @Category(SoapCategory.class)
    public void closeSessionTest() {
        @NotNull final SessionDto session = sessionEndpoint.openSession("admin", "100");
        @NotNull final Result result = sessionEndpoint.closeSession(session);
        Assert.assertTrue(result.isSuccess());
    }

}
