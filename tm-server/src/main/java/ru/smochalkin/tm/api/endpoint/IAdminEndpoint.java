package ru.smochalkin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.dto.result.Result;
import ru.smochalkin.tm.dto.SessionDto;
import ru.smochalkin.tm.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminEndpoint {

    @WebMethod
    Result createUser(
            @WebParam(name = "session") @NotNull SessionDto sessionDto,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    Result removeUserById(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                        @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    Result removeUserByLogin(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                           @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    Result lockUserByLogin(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                         @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    Result unlockUserByLogin(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                           @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    List<UserDto> findAllUsers(@WebParam(name = "session") @NotNull SessionDto sessionDto);

    @WebMethod
    List<SessionDto> findAllSessionsByUserId(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                                             @WebParam(name = "userId") @Nullable String userId
    );

    @WebMethod
    Result closeAllSessionsByUserId(@WebParam(name = "session") @NotNull SessionDto sessionDto,
                                  @WebParam(name = "userId") @Nullable String userId
    );

}
