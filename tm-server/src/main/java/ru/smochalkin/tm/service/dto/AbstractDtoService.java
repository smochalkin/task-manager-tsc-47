package ru.smochalkin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.smochalkin.tm.api.IService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.dto.AbstractEntityDto;

public abstract class AbstractDtoService<E extends AbstractEntityDto> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
